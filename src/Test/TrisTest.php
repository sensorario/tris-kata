<?php

require_once __DIR__ . "/../Tris.php";

class BoardTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->emptyBoard = new Board();

        $this->fullBoard = new Board();

        $monia = new Player("monia");
        $ruggio = new Player("ruggio");

        $this->fullBoard->addPlayer($monia);
        $this->fullBoard->addPlayer($ruggio);
    }

    public function testBoardReturnCellsNumber()
    {
        $cellsNumber = $this->fullBoard->getFreeCells();
        $this->assertSame(9, $cellsNumber);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage no players
     */
    public function testCantMoveWithoutPlayers()
    {
        $this->emptyBoard->move(1);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage invalid move
     */
    public function testMoveNotInAcceptedRange()
    {
        $this->fullBoard->move(mt_rand(9, 9999));
    }

    public function testFreeCellsDecreaseAfterAMove()
    {
        $this->fullBoard->move(mt_rand(0, 8));
        $cellsNumber = $this->fullBoard->getFreeCells();

        $this->assertSame(8, $cellsNumber);
    }

    public function testPlayerHasName()
    {
        $randomString = "ciaone" . rand(1, 42);
        $player1 = new Player($randomString);
        $playerName = $player1->getName();

        $this->assertSame($randomString, $playerName);
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testBoardAcceptsAPlayerOnce()
    {
        $player = new Player("monia");

        $this->fullBoard->addPlayer($player);
        $this->fullBoard->addPlayer($player);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp #.* non può giocare#
     */
    public function testBoard()
    {
        $monia = new Player("monia");
        $ruggio = new Player("ruggio");
        $giance = new Player("pippo");

        $this->fullBoard->addPlayer($monia);
        $this->fullBoard->addPlayer($ruggio);
        $this->fullBoard->addPlayer($giance);
    }

    public function testBoardKnowsCurrentPlayer()
    {
        $monia = new Player("monia");
        $ruggio = new Player("ruggio");

        $this->emptyBoard->addPlayer($monia);
        $this->emptyBoard->addPlayer($ruggio);

        $currentPlayer = $this->emptyBoard->currentPlayer();

        $this->assertSame($monia, $currentPlayer);
    }

    public function testCurrentPlayerChangesAfterMove()
    {

        $firstPlayer = new Player("monia");
        $secondPlayer = new Player("ruggio");

        $this->emptyBoard->addPlayer($firstPlayer);
        $this->emptyBoard->addPlayer($secondPlayer);

        $this->emptyBoard->move(mt_rand(0, 8));

        $currentPlayer = $this->emptyBoard->currentPlayer();

        $this->assertSame($secondPlayer, $currentPlayer);
    }

    public function testCurrentBoard()
    {
        $currentBoard = $this->fullBoard->getBoard();
        $this->assertEquals(array(null, null, null, null, null, null, null, null, null), $currentBoard);
    }

    public function testMove()
    {
        $this->fullBoard->move(3);
        $this->fullBoard->move(4);
        $currentBoard = $this->fullBoard->getBoard();
        $this->assertEquals(array(null, null, null, 0, 1, null, null, null, null), $currentBoard);
    }

    public function testOutputBoard()
    {
        $expectedOutput = <<<OUTPUT
        __|__|__
        __|__|__
          |  |
OUTPUT;

        $output = $this->fullBoard->getOutput();

        $this->assertEquals($expectedOutput, $output);
    }

    public function testOutputBoardAfterMove()
    {
        $expectedOutput = <<<OUTPUT
        __|__|__
        x_|__|__
          |  |
OUTPUT;

        $this->fullBoard->move(3);
        $output = $this->fullBoard->getOutput();

        $this->assertEquals($expectedOutput, $output);
    }

    public function testOutputBoardAfterTwoMoves()
    {
        $expectedOutput = <<<OUTPUT
        __|__|__
        x_|o_|__
          |  |
OUTPUT;

        $this->fullBoard->move(3);
        $this->fullBoard->move(4);
        $output = $this->fullBoard->getOutput();

        $this->assertEquals($expectedOutput, $output);
    }

    public function testTrisNotFound() {

        $trisDone = $this->fullBoard->isTrisDone();
        $this->assertFalse($trisDone);
    }

    public function testTrisFound() {
        $this->fullBoard->move(0);
        $this->fullBoard->move(8);
        $this->fullBoard->move(1);
        $this->fullBoard->move(6);
        $this->fullBoard->move(2);

        $trisDone = $this->fullBoard->isTrisDone();
        $this->assertTrue($trisDone);
    }
}

