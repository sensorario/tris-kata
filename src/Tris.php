<?php

class Board
{
    private $freeCells = 9;
    private $players = [];
    private $cells = array(null, null, null, null, null, null, null, null, null);

    public function getFreeCells()
    {
        return $this->freeCells;
    }

    public function move($index)
    {
        if(count($this->players) != 2) {
            throw new RuntimeException("no players");
        }

        if ($index > 8) {
            throw new RuntimeException("invalid move");
        }

        $this->freeCells--;
        $this->cells[$index] = $this->getFreeCells() % 2;
    }

    public function addPlayer(Player $player)
    {
        if (count($this->players) == 2) {
            throw new \RuntimeException($player->getName() . " non può giocare");
        }

        foreach ($this->players as $item) {
            if ($item === $player) {
                throw new \RuntimeException();
            }
        }

        $this->players[] = $player;
    }

    public function currentPlayer()
    {
        return $this->players[($this->getFreeCells() + 1) % 2];
    }

    public function getBoard()
    {
        return $this->cells;
    }

    public function getOutput()
    {
        for ($i = 0; $i < 9; $i++) {
            $var = "cell" . $i;
            $sign = (($this->cells[$i] % 2) == 0 ? 'x' : 'o');
            $$var = ($this->cells[$i] !== null) ? $sign : '_';
        }

        $output = <<<OUTPUT
        __|__|__
        {$cell3}_|{$cell4}_|__
          |  |
OUTPUT;

        return $output;
    }

    public function isTrisDone() {
       /* var_dump($this->cells[0]);
        var_dump($this->cells[1]);
        var_dump($this->cells[2]);
        var_dump(($this->cells[0] === 0 && $this->cells[1] === 0 && $this->cells[2] === 0));
        die();*/
        return ($this->cells[0] === 0 && $this->cells[1] === 0 && $this->cells[2] === 0);
        $isUnoEqualsDue = ($this->cells[1] == $this->cells[2]);

        return $isZeroEqualsUno&&$isUnoEqualsDue;
        //return ($this->cells[0] == $this->cells[1] && $this->cells[1] == $this->cells[2]);
    }
}

class Player
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}